import functools
import os
import random

import telegram.ext
import psycopg2
import psycopg2.extensions

INTRO_MESSAGE = """Hi,
If you don't understand, please contact us on MakeLab Israel facebook group and we will assist.
שלום, 
זהו הבוט של קבוצת הטלגרם הפעילה של ה-MakeLab.
אין לפרסם בקבוצה פרסומים מסחריים או ספאם בכלל צורותיו.
כדי להיכנס לקבוצה, אנא כתוב/י על עצמך כמה מילים כדוגמת מאיפה את/ה, מה מביא אותך אלינו, מה את/ה מחפש למצוא בקבוצה ואיזה ניסיון תוסיף לקבוצה.
תשובתך תישלח בקבוצה עם הכניסה ותעזור לחברי/ות הקבוצה להכיר אותך.
אנא תאר/י את עצמך:
"""

DESCRIPTION_TOO_SHORT_MESSAGE = """אנא ספקו תיאור כפי שהתבקשתם/ן:"""

MAX_ATTEMPT_COUNT = 3
VERIFICATION_MESSAGE = "מצוין! אנא כתבו בעברית את התשובה לשאלה הבאה:"
RETRY_VERIFICATION_MESSAGE = "תשובה שגויה. אנא כתבו את התשובה לשאלה הבאה:"
VERIFICATION_QUESTIONS = [
    ("כמה זה ארבע כפול חמש?", "עשרים"),
    ("מה היא בירת ישראל?", "ירושלים"),
    ("איזו חיה עושה מיאו?", "חתול")
]

WELCOME_MESSAGE = "ברוכים הבאים, אנא לחצו עכשיו על הלינק כדי להתחבר לקבוצה:"

BAN_MESSAGE = "מצטערים, תשובה זו שגויה. אם לדעתך מדובר בטעות, אנא מצאו אותנו בפייסבוק ושלחו לנו הודעה."
BANNED_MESSAGE = "לצערנו לא עברתם את הזיהוי. אם לדעתך מדובר בטעות, אנא מצאו אותנו בפייסבוק ושלחו לנו הודעה."

ALREADY_DONE_MESSAGE = "כבר קיבלתם קישור!"

GROUP_WELCOME_MESSAGE = """ברוכים הבאים!
הנה מה ש {user} כתב/ה על עצמו/ה:
{description}
"""


def update_callback(update: telegram.update.Update, context: telegram.ext.CallbackContext, updater, chat_id,
                    cursor: psycopg2.extensions.cursor) -> None:
    print(update.message)
    uid = update.effective_user.id

    target_chat = updater.bot.get_chat(chat_id)
    chat: telegram.Chat = updater.bot.get_chat(update.effective_chat.id)
    cursor.execute("SELECT state, attempt, question FROM users WHERE id=%s;", (uid,))
    state = cursor.fetchone()
    if state is None:
        # New user
        print(f"New user: {update.effective_user.id} ({update.effective_user.username})")
        chat.send_message(INTRO_MESSAGE)
        cursor.execute("INSERT INTO users (id, state) VALUES (%s, %s)", (uid, "describe"))
        cursor.connection.commit()
        return

    print(state)
    state, attempt, question = state

    if state == "describe":
        print(f"d:'{update.message.text}' ({len(update.message.text)})")
        if len(update.message.text) < 40:
            print("Description too short")
            update.message.reply_text(DESCRIPTION_TOO_SHORT_MESSAGE)
        else:
            print("Description okay")
            question_index = random.randint(0, len(VERIFICATION_QUESTIONS) - 1)
            cursor.execute(
                "UPDATE users SET state = %s, description = %s, question = %s, attempt = 0 WHERE id=%s", ("maths", update.message.text, question_index, uid)
            )
            cursor.connection.commit()
            chat.send_message(VERIFICATION_MESSAGE)
            chat.send_message(VERIFICATION_QUESTIONS[question_index][0])

    elif state == "maths":
        if update.message.text.strip() == VERIFICATION_QUESTIONS[question][1]:
            cursor.execute("UPDATE users SET state = %s WHERE id=%s", ("done", uid))
            cursor.connection.commit()
            update.message.reply_text(WELCOME_MESSAGE)
            update.message.reply_text(target_chat.create_invite_link(member_limit=1).invite_link)
        elif attempt >= MAX_ATTEMPT_COUNT:
            cursor.execute("UPDATE users SET state = %s WHERE id=%s", ("banned", uid))
            cursor.connection.commit()
            update.message.reply_text(BAN_MESSAGE)
        else:
            # I don't wish to store all previous questions
            # Just guarantee we won't give the same question twice in a row
            question_options = list(range(len(VERIFICATION_QUESTIONS)))
            question_options.remove(question)
            question_index = random.choice(question_options)
            cursor.execute("UPDATE users SET question = %s, attempt = %s WHERE id=%s", (question_index, attempt+1, uid))
            cursor.connection.commit()
            chat.send_message(RETRY_VERIFICATION_MESSAGE)
            chat.send_message(VERIFICATION_QUESTIONS[question_index][0])

    elif state == "done":
        chat.send_message(ALREADY_DONE_MESSAGE)
    elif state == "banned":
        chat.send_message(BANNED_MESSAGE)
    else:
        print(f"Unknown state: {state}")


def group_join_callback(update: telegram.update.Update, context: telegram.ext.CallbackContext, updater, chat_id,
                        cursor: psycopg2.extensions.cursor):
    print(update.message)
    for user in update.message.new_chat_members:
        # TODO: This can be done in one query instead of iterating
        cursor.execute("SELECT description FROM users WHERE id=%s", (user.id,))
        description = cursor.fetchone()
        if description is None:
            print(f"No description found for user '{user.id}'")
            # This should not have happened, theoretically
            return
        update.message.reply_text(
            GROUP_WELCOME_MESSAGE.format(
                user=f"@{user.username}" if user.username and user.username != "None" else user.first_name,
                description=description[0]
            )
        )


def main():
    token = os.environ['TOKEN']
    chat_id = int(os.environ['CHAT_ID'])
    database_url = os.environ['DATABASE_URL']

    db: psycopg2.extensions.connection = psycopg2.connect(database_url, sslmode='require')
    cursor: psycopg2.extensions.cursor = db.cursor()

    updater = telegram.ext.Updater(token)

    updater.dispatcher.add_handler(
        telegram.ext.MessageHandler(
            telegram.ext.Filters.chat_type.private,
            functools.partial(update_callback, updater=updater, chat_id=chat_id, cursor=cursor)
        )
    )
    updater.dispatcher.add_handler(
        telegram.ext.MessageHandler(
            telegram.ext.Filters.status_update.new_chat_members,
            functools.partial(group_join_callback, updater=updater, chat_id=chat_id, cursor=cursor)
        )
    )

    # TODO: Maybe logging? Might be overkill for this
    print("Polling...")
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    exit(main())
